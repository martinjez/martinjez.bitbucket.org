
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
//##############################################################################
function generirajPodatke(stPacienta) {
   var ehrId = "";

  // TODO: Potrebno implementirati
	if(stPacienta == 1)
	{
		ehrId = "fa6f650b-a634-4995-a97a-b5ae2afcf59b";
					$("#generirajEHRiD1").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    ehrId + "</span>");
	}
	else if(stPacienta == 2)
	{
		ehrId = "290f204c-a56f-42cb-b328-2ac61acf412b";
							$("#generirajEHRiD2").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    ehrId + "</span>");
	}
	else if(stPacienta == 3)
	{
		ehrId = "9d3c3f13-3ccf-4aef-8dc9-f0e76e5dbecb";
							$("#generirajEHRiD3").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    ehrId + "</span>");

	}
   return ehrId;
}
//##############################################################################
function uporabnikiKreiraj(stPacienta) {
	var sessionId = getSessionId();
	var ehrId = generirajPodatke(stPacienta);

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				 	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>BMI</th>" + "<th class='text-right'>BMR</th></tr>";
                    var information = "BMR vam pove priporočeno število kalorij, ki jih lahko zaužijete na dan.";
                    var weightArray = [];
//==============================================================================
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {

						        for (var i in res) {
						        	weightArray[i] = res[i].weight;
						        }
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});	
//==============================================================================
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
								var lastBMR;
						        for (var i in res) {
						        	console.log(weightArray[i]/(res[i].height*(res[i].height)));
						        	var BMI = Math.round((weightArray[i]/(res[i].height*(res[i].height)))*100*100);
						        	console.log(parseInt(party.dateOfBirth.substring(0,4)));
						        	var BMR = 10*weightArray[i] + 6.25*res[i].height - (5*(2017 - parseInt(party.dateOfBirth.substring(0,4)))) + 5;
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + BMI + "</td><td class='text-right'>"	+ BMR + " kcal" + "</td>";
                        			lastBMR = " Vaš zadnji izmerjeni BMR je "+BMR+". Priporočamo vam, da pojeste manj kot toliko kalorij, če želite znižati telesno težo. <br /> <br /> Priporočena hrana za izgubo odvečne teže vsebuje:  <br /> Ogljikovihidrati:Krompir in riž <br /> Proteini: Perutnina, ribe, jajca <br /> Zelenjava: Vse vrste, najbolje jih je skuhati na pari. <br /> Maščobe: Ribje olje <br /> <br /> <br /> Hrana, ki se ji izogibajte: <br /> Ogljikovi hidrati: Testenine in kruh <br /> Proteini: Ostale vrste mesa in sir <br /> Zelenjava: Pečena na veliko olja <br /> Maščobe: Ne uporabljajte nobenih pretiranoma. ";
						        }
						        information += lastBMR;
						        results += "</table>";
						        			$("#rezultatMeritveVitalnihZnakov").append("<b><br/><br/><br/><br/><br/><span>Pridobivanje " +
          "podatkov za '" + " BMI in BMR " + "' bolnika '" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        $("#rezultatMeritveVitalnihZnakov").append(information);
						        //charts and diagrams
										
										var BMIstatistics = [
										  { product: 'Normal/underweight <24.9',  count: 31.2 },
										  { product: 'Overweight 25-29.9',  count: 33.1 },
										  { product: 'Obesity 30-39.9', count: 29.4 },
										  { product: 'Extreme obesity 40+', count: 6.3 }
										
										];
										//set up the angles of the pie chart using the pie layout helper
										var pie = d3.layout.pie()
										  .value(function(d) { return d.count })
										
										//connect our data to the slices
										var slices = pie(BMIstatistics);
										
										//size of the pie chart
										var arc = d3.svg.arc()
										  .innerRadius(0)
										  .outerRadius(100);
										
										// another helper that returns a color based on an ID, category10
										var color = d3.scale.category10();
										
										
										var svg = d3.select('svg.pie');
										var g = svg.append('g')
										  .attr('transform', 'translate(300, 100)')
										
										g.selectAll('path.slice')
										  .data(slices)
										    .enter()
										      .append('path')
										        .attr('class', 'slice')
										        .attr('d', arc)
										        .attr('fill', function(d) {
										          return color(d.data.product);
										        });
										
										// building a legend
										svg.append('g')
										  .attr('class', 'legend')
										    .selectAll('text')
										    .data(slices)
										      .enter()
										        .append('text')
										          .text(function(d) { 

										          	return ' • ' + d.data.product + ' (' + d.data.count + ')%';
										          })
										          .attr('fill', function(d) { return color(d.data.product); })
										          .attr('y', function(d, i) { return 30 * (i + 1); })

					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});	
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
//##############################################################################
function uporabniki() {
		uporabnikiKreiraj(1);
		uporabnikiKreiraj(2);
		uporabnikiKreiraj(3);
}
//==============================================================================
function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val();// + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		console.log("WHAT")
		$.ajaxSetup({headers: {"Ehr-Session": sessionId}});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		      
		        };
		       console.log("OK:" + ehrId);
		       $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                              "label label-success fade-in'>Uspešno kreiran EHR '" +
                              ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}
//==============================================================================
function preberiEHRodBolnika() {
	sessionId = getSessionId();
	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}
//##############################################################################
$(document).ready(function() {
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });
//==============================================================================
  $('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
//==============================================================================
	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajVitalnoTelesnaTemperatura").val(podatki[4]);
		$("#dodajVitalnoKrvniTlakSistolicni").val(podatki[5]);
		$("#dodajVitalnoKrvniTlakDiastolicni").val(podatki[6]);
		$("#dodajVitalnoNasicenostKrviSKisikom").val(podatki[7]);
		$("#dodajVitalnoMerilec").val(podatki[8]);
	});
//==============================================================================
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
});

//##############################################################################
function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,

		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "User"
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}
//##############################################################################
function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + " BMI in BMR " + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");

				 	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>BMI</th>" + "<th class='text-right'>BMR</th></tr>";
                    var information = "BMR vam pove priporočeno število kalorij, ki jih lahko zaužijete na dan.";
                    var weightArray = [];   
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {

						        for (var i in res) {
						        	weightArray[i] = res[i].weight;
						        }
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});					 
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
								var lastBMR;
						        for (var i in res) {
						        	console.log(weightArray[i]/(res[i].height*(res[i].height)));
						        	var BMI = Math.round((weightArray[i]/(res[i].height*(res[i].height)))*100*100);
						        	console.log(parseInt(party.dateOfBirth.substring(0,4)));
						        	var BMR = 10*weightArray[i] + 6.25*res[i].height - (5*(2017 - parseInt(party.dateOfBirth.substring(0,4)))) + 5;
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + BMI + "</td><td class='text-right'>"	+ BMR + " kcal" + "</td>";
                        			lastBMR = " Vaš zadnji izmerjeni BMR je "+BMR+". Priporočamo vam, da pojeste manj kot toliko kalorij, če želite znižati telesno težo. <br /> <br /> Priporočena hrana za izgubo odvečne teže vsebuje:  <br /> Ogljikovihidrati:Krompir in riž <br /> Proteini: Perutnina, ribe, jajca <br /> Zelenjava: Vse vrste, najbolje jih je skuhati na pari. <br /> Maščobe: Ribje olje <br /> <br /> <br /> Hrana, ki se ji izogibajte: <br /> Ogljikovi hidrati: Testenine in kruh <br /> Proteini: Ostale vrste mesa in sir <br /> Zelenjava: Pečena na veliko olja <br /> Maščobe: Ne uporabljajte nobenih pretiranoma. ";
						        }
						        information += lastBMR;
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
						        $("#rezultatMeritveVitalnihZnakov").append(information);
						        //charts and diagrams
										
										var BMIstatistics = [
										  { product: 'Normal/underweight <24.9',  count: 31.2 },
										  { product: 'Overweight 25-29.9',  count: 33.1 },
										  { product: 'Obesity 30-39.9', count: 29.4 },
										  { product: 'Extreme obesity 40+', count: 6.3 }
										
										];
										//set up the angles of the pie chart using the pie layout helper
										var pie = d3.layout.pie()
										  .value(function(d) { return d.count })
										
										//connect our data to the slices
										var slices = pie(BMIstatistics);
										
										//size of the pie chart
										var arc = d3.svg.arc()
										  .innerRadius(0)
										  .outerRadius(100);
										
										// another helper that returns a color based on an ID, category10
										var color = d3.scale.category10();
										
										
										var svg = d3.select('svg.pie');
										var g = svg.append('g')
										  .attr('transform', 'translate(300, 100)')
										
										g.selectAll('path.slice')
										  .data(slices)
										    .enter()
										      .append('path')
										        .attr('class', 'slice')
										        .attr('d', arc)
										        .attr('fill', function(d) {
										          return color(d.data.product);
										        });
										
										// building a legend
										svg.append('g')
										  .attr('class', 'legend')
										    .selectAll('text')
										    .data(slices)
										      .enter()
										        .append('text')
										          .text(function(d) { 

										          	return ' • ' + d.data.product + ' (' + d.data.count + ')%';
										          })
										          .attr('fill', function(d) { return color(d.data.product); })
										          .attr('y', function(d, i) { return 30 * (i + 1); })

					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
